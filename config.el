;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-

;; Place your private configuration here! Remember, you do not need to run 'doom
;; refresh' after modifying this file!

(require 'subr-x)
(setenv "SSH_AUTH_SOCK"
        (string-trim
         (shell-command-to-string "gpgconf --list-dirs agent-ssh-socket")))


;; These are used for a number of things, particularly for GPG configuration,
;; some email clients, file templates and snippets.
(setq user-full-name "Andrew Fontaine"
      user-mail-address (if IS-MAC "afontaine@gitlab.com" "andrew@afontaine.ca"))

;; Doom exposes five (optional) variables for controlling fonts in Doom. Here
;; are the three important ones:
;;
;; + `doom-font'
;; + `doom-variable-pitch-font'
;; + `doom-big-font' -- used for `doom-big-font-mode'
;;
;; They all accept either a font-spec, font string ("Input Mono-12"), or xlfd
;; font string. You generally only need these two:
(setq doom-font (font-spec :family "JetBrains Mono" :size 14))

;; There are two ways to load a theme. Both assume the theme is installed and
;; available. You can either set `doom-theme' or manually load a theme with the
;; `load-theme' function. These are the defaults.
(setq doom-theme 'doom-laserwave)

(add-hook! 'after-make-frame-functions
  (defun afontaine/load-theme-fix (frame)
    (select-frame frame)
    (doom/reload-theme)))

;; If you intend to use org, it is recommended you change this!
(setq org-directory "~/org/")
(setq org-roam-directory "~/org/")

;; If you want to change the style of line numbers, change this to `relative' or
;; `nil' to disable it:
(setq display-line-numbers-type t)


;; Here are some additional functions/macros that could help you configure Doom:
;;
;; - `load!' for loading external *.el files relative to this one
;; - `use-package' for configuring packages
;; - `after!' for running code after a package has loaded
;; - `add-load-path!' for adding directories to the `load-path', where Emacs
;;   looks when you load packages with `require' or `use-package'.
;; - `map!' for binding new keys
;;
;; To get information about any of these functions/macros, move the cursor over
;; the highlighted symbol at press 'K' (non-evil users must press 'C-c g k').
;; This will open documentation for it, including demos of how they are used.
;;
;; You can also try 'gd' (or 'C-c g d') to jump to their definition and see how
;; they are implemented.


(set-email-account! "afontaine"
                    '((mu4e-sent-folder . "/afontaine/Sent Items")
                      (mu4e-drafts-folder . "/afontaine/Drafts")
                      (mu4e-trash-folder . "/afontaine/Trash")
                      (mu4e-refile-folder . "/afontaine/Inbox")
                      (mu4e-sent-messages-behavior 'sent)
                      (smtpmail-smtp-user . "andrew@afontaine.ca")
                      (smtpmail-default-smtp-server . "smtp.mailfence.com")
                      (smtpmail-smtp-server . "smtp.mailfence.com")
                      (smtpmail-smtp-service . 465)
                      (smtpmail-stream-type . ssl)
                      (smtpmail-debug-info . t)
                      (smtpmail-smtp-user . "afontaine")
                      (smtpmail-servers-requiring-authorization . "smtp.mailfence.com")
                      (user-mail-address . "andrew@afontaine.ca"))
                    t)

(setq mu4e-update-interval 120)

(after! mu4e
  (add-to-list 'mu4e-bookmarks
              (make-mu4e-bookmark
                :name "Inbox"
                :query "maildir:\"/afontaine/Inbox\""
                :key ?i)))

(use-package! mu4e-alert
  :after mu4e
  :init
  (setq doom-modeline-mu4e t)
  (mu4e-alert-set-default-style (if IS-MAC 'notifier 'notifications))
  (setq mu4e-alert-interesting-mail-query "flag:unread AND maildir:\"/afontaine/Inbox\"")
  (mu4e-alert-enable-notifications)
  (mu4e-alert-enable-mode-line-display))

(use-package! org-books
  :after org
  :init
  (setq org-books-file (concat org-directory "books.org")))


(defvar lsp-elixir--config-options (make-hash-table))

(setq js-indent-level 2)
(setq web-mode-css-indent-offset 2)
(setq web-mode-attr-indent-offset 2)
(setq web-mode-markup-indent-offset 2)
(setq web-mode-code-indent-offset 2)

(add-to-list 'exec-path (concat (getenv "HOME") "/.vim/lsp/elixir-ls/release"))

(add-hook 'js2-mode-hook
          (lambda () (doom/set-indent-width 2)))


(add-hook 'lsp-after-initialize-hook
          (lambda ()
            (lsp--set-configuration `(:elixirLS, lsp-elixir--config-options))))

(add-to-list 'auto-mode-alist '("\\.l?eex\\'" . web-mode))

(setq web-mode-engines-alist '(("vue" . "\\.vue\\'") ("elixir" . "\\.l?eex\\'")))

(after! flycheck
  (flycheck-add-mode 'javascript-eslint 'web-mode)
  (flycheck-add-mode 'css-stylelint 'web-mode)
  (add-hook 'web-mode-local-vars-hook (lambda () (flycheck-add-next-checker 'lsp 'javascript-eslint)))
  (add-hook 'web-mode-local-vars-hook (lambda () (flycheck-add-next-checker 'javascript-eslint 'css-stylelint)))
  (add-hook 'web-mode-hook (lambda () (doom/set-indent-width 2))))

(setq afontaine/test-command nil)

(add-hook 'elixir-mode-hook (lambda ()
                              (when (string-match "_test\\.exs\\'" (buffer-file-name))
                                (setq-local afontaine/test-command "mix test --trace"))))
(add-hook 'js2-mode-hook
          (lambda ()
            (setq-local afontaine/test-command
                        (cond ((string-match "/frontend/.+_spec\\.js\\'" (buffer-file-name)) "yarn jest")
                              ((string-match "/javascripts/.+_spec\\.js\\'" (buffer-file-name)) "yarn karma -f")
                              (t nil)))))

(add-hook 'enh-ruby-mode-hook (lambda ()
                              (when (string-match "_spec\\.rb\\'" (buffer-file-name))
                                (setq-local afontaine/test-command "bundle exec rspec"))))
(add-hook 'ruby-mode-hook (lambda ()
                              (when (string-match "_spec\\.rb\\'" (buffer-file-name))
                                (setq-local afontaine/test-command "bundle exec rspec"))))

(add-hook 'git-commit-mode-hook (lambda () (setq fill-column 72)))

(defun afontaine/register-test-runner (regex command hook map)
  (add-hook hook (lambda ()
                   (when (string-match regex (buffer-file-name))
                     (setq-local afontaine/test-command command)
                     (map! :localleader map
                           :prefix ("t" . "test")
                           :desc "file" "f" #'afontaine/run-test-file
                           :desc "suite" "s" #'afontaine/run-test-suite)))))

(defun afontaine/run-test-file ()
  "Runs tests for the current file"
  (interactive)
  (afontaine/run-test '(lambda (project-folder file cmd) (concat cmd " " (file-relative-name file project-folder)))))

(defun afontaine/run-test-suite ()
  "Runs the entire test suite for the current file"
  (interactive)
  (afontaine/run-test '(lambda (project-folder file cmd) cmd)))

(defun afontaine/run-test (test-cmd)
  "Runs the given test cmd"
  (let ((current-project-folder (projectile-project-root)))
    (cond
     ((eq afontaine/test-command nil) (error "no defined test command"))
     ((eq current-project-folder nil) (error "not in a project"))
     (t
      (let ((cd-command (concat "cd " current-project-folder)))
        (compilation-start (concat cd-command " && " (funcall test-cmd current-project-folder buffer-file-name afontaine/test-command))))))))

(map!
  ;; Window Movements
 "C-h"    #'evil-window-left
 "C-j"    #'evil-window-down
 "C-k"    #'evil-window-up
 "C-l"    #'evil-window-right
 "A-q"    #'delete-window
 "C-`"      #'+popup/toggle
 "<C-tab>"  #'+popup/other)

(map! :leader
      :prefix ("T" . "test")
      :n :desc "file" "f" #'afontaine/run-test-file
      :n :desc "suite" "s" #'afontaine/run-test-suite)

(map! :map doom-leader-open-map :desc "email" "e" #'=mu4e)

(defun org-journal-find-location ()
  ;; Open today's journal, but specify a non-nil prefix argument in order to
  ;; inhibit inserting the heading; org-capture will insert the heading.
  (org-journal-new-entry t)
  ;; Position point on the journal's top-level heading so that org-capture
  ;; will add the new entry as a child entry.
  (goto-char (org-find-exact-headline-in-buffer (org-find-top-headline) (current-buffer) t)))

(after! pretty-code
  (add-to-list '+pretty-code-symbols (:example_block "»" :example_block_end "«")))

(defun org-gitlab-open (path)
  (browse-url
   (concat
    "https://gitlab.com/"
    (when (string-match "\\(.+\\)\\([#!$&]\\)\\([0-9]+\\)" path)
      (concat (match-string 1 path)
              (let ((x (match-string 2 path)))
                (cond
                 ((string= x "#") "/issues/")
                 ((string= x "!") "/merge_requests/")
                 ((string= x "$") "/snippets/")
                 ((string= x "&") "/epics/")))
              (match-string 3 path))))))

(defun org-gitlab-user-open (path) (browse-url (concat "https://gitlab.com/" path)))

(after! org
  (setq org-superstar-headline-bullets-list '("☰" "☱" "☲" "☳" "☴" "☵" "☶" "☷" "☷" "☷" "☷"))
  (setq org-todo-keywords
        '((sequence "TODO(t)" "REVIEW(r)" "PROJ(p)" "STRT(s)" "WAIT(w)" "HOLD(h)" "|" "DONE(d)" "KILL(k)" "MERGED(m)")
        (sequence "[ ](T)" "[-](S)" "[?](W)" "|" "[X](D)")))
  (org-add-link-type "gitlab" 'org-gitlab-open)
  (org-add-link-type "gl-user" 'org-gitlab-user-open)
  (map! :map org-mode-map
        :n "M-j" #'org-metadown
        :n "M-k" #'org-metaup))

(after! org-capture
  (map! :map org-capture-mode-map
        :localleader
        :n "d" #'org-deadline
        :n "s" #'org-schedule)
  (setq org-capture-templates
    '(("t" "Todo" entry (file+headline "~/org/todo.org" "Tasks")"* TODO %?\n  %i\n  %a")
      ("n" "Personal notes" entry (file+headline +org-capture-notes-file "Inbox") "* %u %?\n%i\n%a" :prepend t)
      ("j" "Journal templates")
      ("je" "Entry" entry (function org-journal-find-location) "** %(format-time-string org-journal-time-format)%^{Title} %^g\n%i%?")
      ("jt" "Todo" plain (function org-journal-find-location) "** TODO %^{Title} %^g\n%i%?")
      ("b" "Book" entry (file org-books-file)
        "%(let* ((url (substring-no-properties (current-kill 0)))
                      (url-type (org-books-get-url-type url org-books-url-patterns))
                      (details (if url-type (org-books-get-details url url-type))))
                (when details (apply #'org-books-format 1 details)))")
      ("o" "Centralized templates for projects")
      ("ot" "Project todo" entry #'+org-capture-central-project-todo-file "* TODO %?\n %i\n %a" :heading "Tasks" :prepend nil)
      ("on" "Project notes" entry #'+org-capture-central-project-notes-file "* %U %?\n %i\n %a" :heading "Notes" :prepend t)
      ("oc" "Project changelog" entry #'+org-capture-central-project-changelog-file "* %U %?\n %i\n %a" :heading "Changelog" :prepend t)
      ("c" "Cooking Recipes")
      ("cc" "Cookbook" entry (file+headline "~/org/cookbook.org" "Inbox") "%(org-chef-get-recipe-from-url)" :empty-lines 1)
      ("cm" "Manual Cookbook" entry (file+headline "~/org/cookbook.org" "Inbox")
       "* %^{Recipe title: }\n  :PROPERTIES:\n  :source-url:\n  :servings:\n  :prep-time:\n  :cook-time:\n  :ready-in:\n  :END:\n** Ingredients\n   %?\n** Directions\n\n"))))

(after! org-journal
  (setq org-journal-enable-agenda-integration t)
  (customize-set-variable 'org-journal-file-format "%Y-%m-%d.org")
  (customize-set-variable 'org-journal-date-format "%Y-%m-%d %A")
  (setq org-journal-carryover-items "TODO=\"TODO\"|TODO=\"REVIEW\""))


(defvar fancy-splash-image-template
  (expand-file-name "logo.svg" doom-private-dir)
  "Default template svg used for the splash image, with substitutions from ")
(defvar fancy-splash-image-nil "" "An image to use at minimum size, usually a transparent pixel")

(setq fancy-splash-sizes
  `((:height 500 :min-height 50 :padding (0 . 2) :template ,(expand-file-name "logo.svg" doom-private-dir))
    (:height 440 :min-height 42 :padding (1 . 4) :template ,(expand-file-name "logo.svg" doom-private-dir))
    (:height 400 :min-height 38 :padding (1 . 4) :template ,(expand-file-name "logo.svg" doom-private-dir))
    (:height 350 :min-height 36 :padding (1 . 3) :template ,(expand-file-name "logo.svg" doom-private-dir))
    (:height 300 :min-height 34 :padding (1 . 3) :template ,(expand-file-name "logo.svg" doom-private-dir))
    (:height 250 :min-height 32 :padding (1 . 2) :template ,(expand-file-name "logo.svg" doom-private-dir))
    (:height 200 :min-height 30 :padding (1 . 2) :template ,(expand-file-name "logo.svg" doom-private-dir))
    (:height 100 :min-height 24 :padding (1 . 2) :template ,(expand-file-name "logo.svg" doom-private-dir))
    (:height 0   :min-height 0  :padding (0 . 0) :file ,fancy-splash-image-nil)))

(defvar fancy-splash-sizes
  `((:height 500 :min-height 50 :padding (0 . 2))
    (:height 440 :min-height 42 :padding (1 . 4))
    (:height 330 :min-height 35 :padding (1 . 3))
    (:height 200 :min-height 30 :padding (1 . 2))
    (:height 0   :min-height 0  :padding (0 . 0) :file ,fancy-splash-image-nil))
  "list of plists with the following properties
  :height the height of the image
  :min-height minimum `frame-height' for image
  :padding `+doom-dashboard-banner-padding' to apply
  :template non-default template file
  :file file to use instead of template")

(defvar fancy-splash-template-colours
  '(("$colour1" . keywords) ("$colour2" . type) ("$colour3" . base5) ("$colour4" . base8))
  "list of colour-replacement alists of the form (\"$placeholder\" . 'theme-colour) which applied the template")

(unless (file-exists-p (expand-file-name "theme-splashes" doom-cache-dir))
  (make-directory (expand-file-name "theme-splashes" doom-cache-dir) t))

(defun fancy-splash-filename (theme-name height)
  (expand-file-name (concat (file-name-as-directory "theme-splashes")
                            (symbol-name doom-theme)
                            "-" (number-to-string height) ".svg")
                    doom-cache-dir))

(defun fancy-splash-clear-cache ()
  "Delete all cached fancy splash images"
  (interactive)
  (delete-directory (expand-file-name "theme-splashes" doom-cache-dir) t)
  (message "Cache cleared!"))

(defun fancy-splash-generate-image (template height)
  "Read TEMPLATE and create an image if HEIGHT with colour substitutions as  ;described by `fancy-splash-template-colours' for the current theme"
    (with-temp-buffer
      (insert-file-contents template)
      (re-search-forward "$height" nil t)
      (replace-match (number-to-string height) nil nil)
      (dolist (substitution fancy-splash-template-colours)
        (beginning-of-buffer)
        (while (re-search-forward (car substitution) nil t)
          (replace-match (doom-color (cdr substitution)) nil nil)))
      (write-region nil nil
                    (fancy-splash-filename (symbol-name doom-theme) height) nil nil)))

(defun fancy-splash-generate-images ()
  "Perform `fancy-splash-generate-image' in bulk"
  (dolist (size fancy-splash-sizes)
    (unless (plist-get size :file)
      (fancy-splash-generate-image (or (plist-get size :file)
                                       (plist-get size :template)
                                       fancy-splash-image-template)
                                   (plist-get size :height)))))

(defun ensure-theme-splash-images-exist (&optional height)
  (unless (file-exists-p (fancy-splash-filename
                          (symbol-name doom-theme)
                          (or height
                              (plist-get (car fancy-splash-sizes) :height))))
    (fancy-splash-generate-images)))

(defun get-appropriate-splash ()
  (let ((height (frame-height)))
    (cl-some (lambda (size) (when (>= height (plist-get size :min-height)) size))
             fancy-splash-sizes)))

(setq fancy-splash-last-size nil)
(setq fancy-splash-last-theme nil)
(defun set-appropriate-splash (&optional frame)
  (let ((appropriate-image (get-appropriate-splash)))
    (unless (and (equal appropriate-image fancy-splash-last-size)
                 (equal doom-theme fancy-splash-last-theme)))
    (unless (plist-get appropriate-image :file)
      (ensure-theme-splash-images-exist (plist-get appropriate-image :height)))
    (setq fancy-splash-image
          (or (plist-get appropriate-image :file)
              (fancy-splash-filename (symbol-name doom-theme) (plist-get appropriate-image :height))))
    (setq +doom-dashboard-banner-padding (plist-get appropriate-image :padding))
    (setq fancy-splash-last-size appropriate-image)
    (setq fancy-splash-last-theme doom-theme)
    (+doom-dashboard-reload)))

(add-hook 'window-size-change-functions #'set-appropriate-splash)
(add-hook 'doom-load-theme-hook #'set-appropriate-splash)
